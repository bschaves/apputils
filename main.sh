#!/usr/bin/env bash
#
#
#
#
#

readonly this_file_main=$(readlink -f "$0")
readonly local_root_dir=$(dirname "$_this_file_main")
readonly file_requirements="${local_root_dir}/requirements.txt"
readonly dir_apputils="${local_root_dir}/apputils"

function run(){
    cd "$local_root_dir"
    [[ ! -x "$dir_apputils/__main__.py" ]] && chmod +x "${dir_apputils}"/__main__.py
    "$dir_apputils/__main__.py" "$@"
}

function configure_app(){
    pip3 install -r "$file_requirements"
}

function main() {

    if [[ -z $1 ]]; then
        run "$@"
    fi

    case "$1" in
        -c|--configure) configure_app;;
    esac
    
}


main "$@"