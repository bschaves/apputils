#!/usr/bin/env python3
#

import os
import sys
import platform
import shutil
import json
from pathlib import Path
from time import sleep


# Status da ultima operação em forma de texto.
current_output: str = None
current_status: bool = False
_global_sleep = 0.15

def global_sleep():
    sleep(_global_sleep)

class StatusOutput(object):
    def __init__(self):
        self.__current_status: bool = False
        self.__current_output: str = None

    def get_output(self) -> str:
        return self.__current_output

    def get_status(self) -> bool:
        return self.__current_status

    def update(self, *, output: str, status:bool):
        self.__current_output = output
        self.__current_status = status

    def update_output(self, output: str):
        self.__current_output = output

    def update_status(self, status:bool):
        self.__current_status = status

status_output_info: StatusOutput = StatusOutput()


class StdoutUtils(object):
    def __init__(self) -> None:
        pass

    def get_terminal_colums(self) -> int:
        try:
            return os.get_terminal_size()[0]
        except:
            return 80

    def print_line(self, char:str='-'):
        print(char * self.get_terminal_colums())


stdout_utils = StdoutUtils()

def get_abspath(path: str) -> str:
    return os.path.abspath(path)

def mkdir(path: str) -> None:
    try:
        os.makedirs(path)
    except Exception as e:
        current_output = e
        current_status = False

        status_output_info.update(
            output=e,
            status=False
        )
    else:
        status_output_info.update_status(True)


def get_user_home_dir():
    return get_abspath(Path().home())


def delete_new_line(list_of_strings: list, char:str=' ') -> list:
    """
        Apaga as quebras de linhas de uma lista, e substitui as quebras de linhas por outro caracter
    em seguida, retorna um nova lista.

    O caracter padrão é um ' '.
    """
    _new_list: list = []
    _new_line = '\n'

    if not isinstance(list_of_strings, list):
            raise Exception(f'{list_of_strings} -> não é uma do tipo lista')

    for element in list_of_strings:
        
        try:
            _new_list.append(element.replace(_new_line, ' '))
        except Exception as e:
            print(e)
            sleep(_global_sleep)
    return _new_list


class dirpath(object):
    def __init__(self, path_dir:str, *, create=False):
        self.path_dir: str = path_dir
        self.create: bool = create
        self._current_status: bool = False
        self._current_output: str = None
        self.path: Path = Path(self.path_dir)

        if create:
            self.mkdir()

    def absolute(self) -> str:
        return get_abspath(self.path_dir)

    def mkdir(self):
        #self.path.mkdir()
        mkdir(self.absolute())
    
    def get_content_files(self, content_in_file: str='*') -> list:
        """
            Retorna os arquivos nos diretórios e subdiretórios.
         
        """
        _files = self.path.rglob(content_in_file)
        _new_list = []
        f: Path = None
        for f in _files:
            if f is None:
                break

            if f.is_file:
                _new_list.append(FilePath(get_abspath(f.absolute())))
        return _new_list

class DirPath(dirpath):
    def __init__(self, path_dir: str, *, create=False):
        super().__init__(path_dir, create=create)

    def concat_dir(self, new_dir: str) -> dirpath:
        return dirpath(os.path.join(self.absolute(), new_dir))

class ListDirs(object):
    def __init__(self):
        """
        Lista de objetos do tipo DirPath.
        """
        self.__dirs: list = []

    def add(self, dir: DirPath):
        self.__dirs.append(dir)

    def delet(self, num: int):
        del self.__dirs[num]

    def clear(self):
        self.__dirs.clear()


class FilePath(object):
    def __init__(self, filename: str):
        self.filename = filename
        self.path: Path = Path(self.filename)

    def dirname(self):
        return os.path.dirname(self.absolute())

    def basename(self):
        return os.path.basename(self.absolute())

    def absolute(self):
        return get_abspath(self.filename)

    def extension(self) -> str:
        return self.path.suffix


class data_text(object):
    def __init__(self, data='') -> None:
        """
            Tipo de dado para ser escrito em um arquivo de texto.
        pode ser uma lista ou string.
        """
        self.data = data
        self.type_data: str = None
        self.header: list = [] # Armazenas as chaves do dicionário, caso self.data for do tipo dict
        self.__separator = '\t'

        if isinstance(self.data, list):
            self.type_data = 'list'
        elif isinstance(self.data, tuple):
            self.type_data = 'tuple'
        elif isinstance(self.data, dict):
            self.type_data = 'dict'
            self.header = self.data.keys()
        elif isinstance(self.data, str):   
            self.type_data = 'string'
        else:
            raise Exception(f'{__class__.__name__} tipo de dado inválido {type(self.data)}')
            return

    def to_string(self) -> str:
        """
            Retorna o valor deste objeto em formato string
        """
        if self.type_data == 'string':
            return self.data
        
        if self.data == '':
            return ''
        
        if self.type_data == 'list':
            return self.__list_to_string(self.data)
        elif self.type_data == 'tuple':
            return self.__list_to_string(list_value=list(self.data))
        elif self.type_data == 'dict':
            return self.__list_to_string(list_value=list(self.data.values()))
    
    def __list_to_string(self, *, list_value: list, separator:str=' ') -> str:
        _new_string: str = ''

        if list_value == []:
            return _new_string
        
        for element in list_value:
            _new_string += str(f'{element}{separator}')
            
        return _new_string
    
    def __string_to_csv(self, *, string_value: str, separator='\t') -> str:
        _new_string: str = ''
        for element in string_value.split():
            _new_string += str(f'{element}{separator}')
        return _new_string
        
    def to_string_csv(self, *, separator:str='\t') -> str:
        """
            Retorna o conteúdo em formato string para CSV
        """
        
        if self.data == '':
            return ''
        
        if self.type_data == 'string':
            return self.__string_to_csv(string_value=self.data, separator=separator)
        elif self.type_data == 'list':
            return self.__list_to_string(list_value=self.data, separator=separator)
        elif self.type_data == 'tuple':
            return self.__list_to_string(list_value=list(self.data), separator=separator)
        elif self.type_data == 'dict':
            return self.__list_to_string(list_value=list(self.data.values()), separator=separator)
    
    def to_list(self) -> list:
        if self.type_data == 'list':
            return self.data
        elif self.type_data == 'tuple':
            return list(self.data)
        elif self.type_data == 'dict':
            return list(self.data.values())
        elif self.type_data == 'string':
            if self.data == '':
                return []
            return self.data.split()
    

class DataText(data_text):
    def __init__(self, data='') -> None:
        super().__init__(data)

    def get_data_text_csv(self, *, separator: str='\t') -> data_text:
        return data_text(self.to_string_csv())



class FileText(FilePath):
    def __init__(self, filename: str):

        super().__init__(filename)
        self.filename = filename
        self.__file_lines: list = []

    def get_line(self) -> str:
        return self.path.read_text(encoding='utf8')

    def get_lines(self, separator: str='\n') -> str:
        return self.get_line().split(separator)
    
    def get_data_text(self) -> DataText:
        return DataText(self.get_line())
    
    def __write_lines_in_file(self, lines: list, open_mode: str='a+'):

        try:
            with open(self.absolute(), open_mode) as file:
                for line in lines:
                    file.write(f'{line}\n')
        except Exception as e:
            print(e)

    def __write_string_in_file(self, string: str, open_mode:str='a+'):
        try:
            with open(self.absolute(), open_mode) as file:
                file.write(f'{string}\n')
        except Exception as e:
            print(e)

    def write_text(self, text: DataText):
        if text.type_data == 'list':
            self.__write_lines_in_file(text.data, open_mode='w')
        elif text.type_data == 'string':
            self.path.write_text(text.data)

    def append_text(self, text: DataText):
        if text.type_data == 'list':
            self.__write_lines_in_file(text.data, open_mode='a+')
        elif text.type_data == 'string':
            self.__write_string_in_file(text.data, open_mode='a+')

class FindText(object):
    def __init__(self, *, text:DataText) -> None:
        self.text: DataText = text

        if not isinstance(self.text, DataText):
            raise Exception(f'{__class__.__name__} -> o texto informado não é do tipo DataText')
            return
        
        #self.text_list: list = self.text.to_list()

    def count_values(self, element) -> int:
        """
            retorna o número de vezes em que o elemento for encontrado na lista
        """
        return self.text.to_list().count(element)
    
    def count_chars(self, char) -> int:
        """
            retorna o número de vezes em que um caracter é encontrado nos itens da lista.
        """
        _count = 0
        for item in self.text.to_list():
            if char in item:
                _count += 1
        return _count
    
    def count_chars_positions(self, char) -> list:
        """
            retorna os números das posições da lista em que char for encontrado.

        Ex:
            # >>> exemplo = FindText(['python', 'android', 'programação'])
            # >>> print(exemplo.count_chars_positions('o'))
            # >>> [0, 1, 2]

            o caracter 'o' foi encontrado nas posições 0, 1 e 2.
        """
        _char_count = []
        for num, item in enumerate(self.text.to_list()):
            if char in item:
                _char_count.append(num)
        return _char_count
    
    def find_text(self, *, text, max_cont: int, ignore_case=False) -> list:
        """
            Retorna uma lista para todas as ocorrências encontradas.
        """
        _count = 1
        _values = []
        if ignore_case:
            try:
                text = text.lower()
            except:
                print(f'{__class__.__name__} -> [text não é do tipo string]')

        for num, item in enumerate(self.text.to_list()):
            new_item = item
            if ignore_case:
                try:
                    new_item = item.lower()
                except:
                    print(f'{__class__.__name__} -> [o elemento {item} não é do tipo string]')

            if text in item:
                _values.append(item)
                if _count == max_cont:
                    break
                _count += 1
        return _values


class DataJson(object):
    def __init__(self, data_json='') -> None:
        self.data_json: str = data_json

        if isinstance(self.data_json, str):
            self.type_json = 'string'
        elif isinstance(self.data_json, dict):
            self.type_json = 'dict'
        else:
            raise Exception(f'{__class__.__name__} -> data_json deve ser do tipo dict ou str')
            return

    def to_string(self) -> str:
        """
            Converter self.data_json para string
        """
        if self.type_json == 'string':
            return self.data_json
        
        if self.data_json == {}:
            return ''
        return json.dumps(self.data_json, indent=4, ensure_ascii=False,)    
    
    def to_dict(self) -> dict:
        if self.type_json == 'dict':
            return self.data_json
        
        if self.data_json == '':
            return {}
        return json.loads(self.data_json)
        
    def dict_to_json(self, data_dict: dict) -> str:
        """
            Recebe um dicionário python e converte em dado JSON.

            Retorna um JSON do tipo string python.
        """
        return json.dumps(data_dict, indent=4, ensure_ascii=False,)
    
    def keys(self) -> list:
        return list(self.to_dict().keys())
    
    def values(self) -> list:
        return list(self.to_dict().values())
    
    def to_data_text(self, *, separator=';') -> DataText:
        _header = [f'KEY{separator}VALUE']
        _list_values = []
        _dict_value = self.to_dict()

        if _dict_value == {}:
            return DataText('')

        for _key in _dict_value.keys():
            _value = _dict_value[_key]
            _list_values.append(f'{_key}{separator}{_dict_value[_key]}')
        return DataText(_list_values)
    

class FileJson(FilePath):
    def __init__(self, filename: str):
        super().__init__(filename)

    def get_content(self):
        return self.get_file_data_json().to_string()
        
    def get_file_data_json(self) -> DataJson:
        """
            Retorna um objeto DataJson apartir do conteúdo do arquivo.
        """
        # Converter o dicionário para formato string.
        # json.dumps(_content_file, indent=4, ensure_ascii=False,)

        _content = DataJson('')
        try:
            with open(self.absolute(), 'rt', encoding='utf8') as jfile:
                _content = json.load(jfile) # Texto do arquivo para dicionário.
        except Exception as e:
            print(__class__.__name__, e)
            return _content
        else:
            return DataJson(_content)
        
    def __write_data_in_file(self, data_json: DataJson, open_mode='a+'):
        """
            Escreve os dados JSON em self.absolute()
        """
        
        if not isinstance(data_json, DataJson):
            raise Exception(f'{__class__.__name__} -> {TypeError}')
            return
        
        try:
            with open(self.absolute(), open_mode, encoding='utf8') as file_json:
                json.dump(
                            data_json.to_dict(), 
                            file_json, 
                            ensure_ascii=False, 
                            sort_keys=True, 
                            indent=4
                            )
        except Exception as e:
            raise Exception(f'{__class__.__name__} {e}')

    def write(self, *, data_json: DataJson):
        """
            Escreve os dados JSON em self.absolute()
        """
        self.__write_data_in_file(data_json, open_mode='w')

    def append(self, *, data_json: DataJson):
        """
            Escreve os dados JSON em self.absolute()
        """
        self.__write_data_in_file(data_json, open_mode='a+')

    def to_file_text(self):
        """
            Escreve o conteúdo em um novo arquivo de texto, no mesmo diretório atual 
        porém com a extensão .txt
        """
        new_file_name = self.basename().replace(self.path.suffix, '.txt')
        local_dir = get_abspath(self.dirname())
        new_file_path = FileText(os.path.join(local_dir, new_file_name))
        new_file_path.write_text(self.get_file_data_json().to_data_text())
        

class UserFileSystem(object):
    def __init__(self) -> None:
        
        self.dir_home: DirPath = DirPath(get_user_home_dir())
        self.dir_downloads: DirPath = DirPath(self.dir_home.concat_dir('Downloads'))
        self.dir_bin: DirPath = DirPath(self.dir_home.concat_dir('bin'))

    def create(self):
        self.dir_home.mkdir()
        self.dir_downloads.mkdir()
        self.dir_bin.mkdir()




def main():
    pass

if __name__ == '__main__':
    main()