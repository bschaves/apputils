#!/usr/bin/env python3

__version__ = '2024-06-01'
__author__ = 'Bruno Chaves'
__repo__ = 'https://gitlab.com/bschaves/apputils'
__download_file__ = 'https://gitlab.com/bschaves/apputils/-/archive/main/apputils-main.zip'