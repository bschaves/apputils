#!/usr/bin/env python3
#

"""
#===================================================================#
# CONFIGURAÇÃO
#===================================================================#
    sudo apt install python3-venv
    python3 -m venv local_venv

"""

import os
import sys
import argparse
import platform
import shutil
import json
from pathlib import Path
from time import sleep

__version__ = '1.1.0'

__this_file__ = os.path.realpath(__file__)
dir_of_project = os.path.abspath(os.path.dirname(__this_file__))
dir_root = os.path.abspath(os.path.join(dir_of_project, '..'))
file_config = os.path.join(dir_root, 'config.json')

sys.path.insert(0, dir_of_project)

try:
    import pandas
except Exception as e:
    print(e)
    sys.exit(1)

from soup_data import (appActions)

parser = argparse.ArgumentParser(description='Filtra e manipula dados em arquivos excel, csv e json')

#======================================================#
# Classe que contém algumas informações recebidas
# da linha de comando.
#======================================================#
class InputArgsFromCli(object):
    def __init__(self) -> None:
        pass
        


#======================================================#
# Main
#======================================================#
def main():


    parser.add_argument(
        '-v', '--version',
        action='version',
        version=__version__,
    )

    parser.add_argument(
        '-i', '--input',
        action='store',
        nargs=1,
        dest='input_file',
        #required=True,
        type=str,
        help='Arquivo com os dados a serem filtrados.'
    )

    parser.add_argument(
        '-o', '--output',
        action='store',
        nargs=1,
        dest='output_file',
        type=str,
        help='Arquivo onde os dados serão salvos.'
    )

    parser.add_argument(
        '-c', '--column',
        action='store',
        nargs=1,
        dest='column',
        type=str,
        help='Coluna onde estão os dados a serem filtrados.'
    )

    # Procura por um texto e gera um arquivo de saída
    parser.add_argument(
        '--find',
        action='store',
        nargs=1,
        dest='find',
        type=str,
        help='Texto a ser filtrado.'
    )

    # Procura por mais de um texto e gera mais de um arquivo de saída.
    parser.add_argument(
        '--finds',
        action='store',
        nargs='*',
        dest='finds',
        type=str,
        help='Textos a serem filtrados.'
    )

    parser.add_argument(
        '-e', '--export',
        action='store',
        nargs=1,
        type=str,
        dest='type_file',
        help='Seleciona o tipo de arquivo a ser exportado |excel|csv|json.'
    )

    parser.add_argument(
        '--sep',
        action='store',
        nargs=1,
        type=str,
        dest='separator',
        help='Separador de colunas em arquivo CSV'
    )

    parser.add_argument(
        '--header',
        action='store_true',
        dest='header',
        help='Exibe o cabeçalho do arquivo e sai.'
    )


    parser.add_argument(
        '--del-column',
        action='store',
        nargs=1,
        dest='del_column',
        type=str,
        help='Remove uma coluna'
    )

    parser.add_argument(
        '--file-config',
        action='store_true',
        dest='file_config',
        help='Ignora os argumentos da linha de comando e usa o arquivo de configuração',
    )

    cli_args = parser.parse_args()
    appActions(options=cli_args.__dict__)
    
if __name__ == '__main__':
    main()