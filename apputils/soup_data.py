#!/usr/bin/env python3
#
#
#
# https://pythonspot.com/read-excel-with-pandas/
# https://www.cybrosys.com/blog/how-to-convert-csv-to-excel-using-pandas-in-python
# https://www.covildodev.com.br/artigo/remover-item-dicionario-python
# https://www.analisededados.blog.br/2020/04/importando-excel-no-python-pandas.html
#
#

from common import (
    DataJson,
    DataText,
    FilePath,
    FileText,
    FileJson,
    FindText,
    DirPath,
    ListDirs,
    UserFileSystem,
    get_abspath,
    get_user_home_dir,
    StdoutUtils,
)

import os
import sys
import pandas

from pandas.core.series import (Series)
from pandas import (
    ExcelFile, 
    ExcelWriter,
    DataFrame,
    Timestamp
)

from numpy import (ndarray)

stdout_util = StdoutUtils()

#===========================================================#
# REPRESENTAÇÃO DE UM DADO GENÉRICO, GERALMENT É UMA STRING
#===========================================================#
class GenericData(object):
    def __init__(self, generic_data) -> None:
        self.generic_data = generic_data

    @property
    def generic_data(self):
        return self._generic_data
    
    @generic_data.setter
    def generic_data(self, new_data):
        self._generic_data = new_data

    def dataType(self):
        return type(self.generic_data)
    
    def toString(self) -> str:
        try:
            return str(self.generic_data)
        except Exception as e:
            print(f'[{__class__.__name__}] =>> {e}')
            return  self.dataType
        
    def isNone(self) -> bool:
        return self.generic_data is None
    
    def addItem(self, *, item: str, separator_item: str='-'):
        self.generic_data = str(self.generic_data)
        try:
            self.generic_data += f'{separator_item}{item}'
        except Exception as e:
            stdout_util.print_line('-')
            print(f'[{__class__.__name__}] -> {e}')

#===========================================================#
# REPRESENTAÇÃO DE UMA LINHA DA PLANILHA
#===========================================================#
class SheetRow(object):
    def __init__(self, *, sheet_line: list, num_line: int, separator:str=' ') -> None:
        if not isinstance(sheet_line, list):
            raise Exception(f'[ERRO] -> {__class__.__name__} uma linha precisa ser do tipo [list]')
            sys.exit(1)

        if not isinstance(num_line, int):
            raise Exception(f'[ERRO] -> {__class__.__name__} o número da linha precisa ser do tipo [int]')
            sys.exit(1)

        if not isinstance(separator, str):
            raise Exception(f'[ERRO] -> {__class__.__name__} o separador de texto da linha precisa ser do tipo [str]')
            sys.exit(1)

        self.sheet_line: list = sheet_line
        self.sheet_numline: int = num_line
        self.sheet_separator:str = separator
        
    @property
    def sheet_line(self) -> list:
        return self._sheet_line
    
    @sheet_line.setter
    def sheet_line(self, new_sheet_line):
        if new_sheet_line is None:
            self._sheet_line = self.sheet_line
        elif not isinstance(new_sheet_line, list):
            self._sheet_line = self.sheet_line
        else:
            self._sheet_line = new_sheet_line

    @property
    def separator(self) -> str:
        return self._separator
    
    @separator.setter
    def separator(self, new_separator:str):
        if not isinstance(new_separator, str):
            print(f'[ERRO] -> separador inválido, definindo o padrão: " "')
            self._separator = ' '
        else:
            self._separator = new_separator
    
    def getLineToString(self) -> str:
        """
            Conveter e retorna a linha atual em forma de string
        os elementos são separados pelo self.separator.
        """
        if self.sheet_line == []:
            return ' '
    
        try:
            return self.sheet_separator.join(self.sheet_line)
        except Exception as e:
            print(e)
            stdout_util.print_line('-')
            print(f'[ERRO] -> linha/planilha {self.sheet_numline}')
            stdout_util.print_line('-')
            return ' '
        
    def numColumns(self) -> int:
        return len(self.sheet_line)
    
    def getElementAt(self, num: int) -> GenericData:
        """
            Retorna o elemento da linha na posição num
        o retorno é um dado do tipo GenericData.
        """
        try:
            return GenericData(self.sheet_line[num])
        except Exception as e:
            print(e)
            print(f'[{__class__.__name__}] número inválido para a linha atual: num {num}')
            return -1
    
    def contains(self, text: str) -> bool:
        """
            Verifica se a ocorrência text existe na linha.
        """
        _contains = False
        for item in self.sheet_line:
            if text in str(item):
                _contains = True
                break
        return _contains
    
    def containsIqual(self, text: str) -> bool:
        """
            Verifica se existe um elemento igual a text na linha,
        para essa comparação os dados são convertidos em string.
        """
        _contains = False
        for item in self.sheet_line:
            if text == str(item):
                _contains = True
                break
        return _contains
    
    def removeItemAt(self, num: str):
        """
            Apaga um elemento da linha/lista com base no indice
        """
        try:
            del self.sheet_line[num]
        except Exception as e:
            stdout_util.print_line('-')
            print(f'[{__class__.__name__}] -> {e}')
            stdout_util.print_line('-')

    def removeItemIqual(self, text: str):
        """
            Remove um item da lista se ele for igual a text
        """
        for num, item in enumerate(self.sheet_line):
            if text == item:
                del self.sheet_line[num]

    def removeItems(self, items: list):
        """
            Recebe uma lista de items para remover da lista, se os 
        items existirem na lista eles serão removidos.
        """
        for item in items:
            self.removeItemIqual(item)

    def addItem(self, item: str):
        """
            Inclui um novo item na linha, semelhante a criar uma nova coluna no excel.
        """
        try:
            self.sheet_line.append(item)
        except Exception as e:
            stdout_util.print_line('-')
            print(f'[{__class__.__name__}] -> {e}')
            stdout_util.print_line('-')
   
    def concatItems(self, nums: list):
        """
            Recebe uma lista de índices para serem concatenados, 
        os items concatenados representarão um novo elemento/string 
        que será adicionado no final da linha/coluna
        """

        # Para concatenar itens, é necerrário receber pelo menos dois itens.
        if len(nums) < 2:
            print(f'[{__class__.__name__}] -> ERRO para concatenar, é necessário pelo menos 2 itens')
            print(len(nums))
            return
        
        list_new_items = []

        # Pegar os elementos da linha, de acordo com os índices passados no parâmetro [nums]        
        for num in nums:
            list_new_items.append(self.sheet_line[num])

        # Gerar um objeto GenericData para concatenar e incluir os items na lista.
        _generic_data: GenericData = None
        for new_item in list_new_items:
            if _generic_data is None:
                _generic_data = GenericData(new_item)
                continue
            _generic_data.addItem(item=str(new_item))
        
        self.addItem(_generic_data.toString())
        

#===========================================================#
# LISTA DE LINHAS
#===========================================================#
class ListRows(object):   # list()      # list<SheetRow>
    def __init__(self, *, header: list, lines: list, separator: str=' ') -> None:

        if not isinstance(header, list):
            raise Exception(f'{__class__.__name__} -> o cabeçalho precisa ser do tipo list')
        
        if not isinstance(lines, list):
            raise Exception(f'{__class__.__name__} -> as linhas precisam ser do tipo list')
        
        if not isinstance(separator, str):
            raise Exception(f'{__class__.__name__} -> o separador precisa ser do tipo str')
            
        self.header: list = header # list()
        self.lines: list = lines   # list<SheetRow>
        self.separator: str = separator

    @property
    def lines(self) -> list:
        return self._lines
    
    @lines.setter
    def lines(self, new_lines: list):
        if not isinstance(new_lines, list):
            self._lines = self.lines
        else:
            self._lines = new_lines

    @property
    def header(self):
        return self._header
    
    @header.setter
    def header(self, new_header: list):
        if not isinstance(new_header, list):
            self._header = self.header
        else:
            self._header = new_header

    def __addItemInHeader(self, new_item: str):
        self.header.append(new_item)

    def hederToRow(self) -> SheetRow:
        """
            Retorna o cabeçalho em uma representação do tipo SheetRow.
        """
        return SheetRow(sheet_line=self.header, num_line=0, separator=self.separator)
    
    def headerToString(self) -> str:
        return self.separator.join(self.header)
    
    def indexInColumn(self, column_name: str) -> int:
        """
            Retorna a posição de um item da coluna.
        """
        try:
            return self.header.index(column_name)
        except:
            print(f'[{__class__.__name__}] ERRO a coluna [{column_name}] não existe no cabeçalho da planilha')
            return -1

    def concatColumns(self, columns: list):
        """
            Recebe uma lista de strings/colunas, se os itens da lista existirem
        no cabeçalho, serão concatenados no cabeçalho e também nas respectivas linhas,
        ou sejá, é semelhante a função concatenar do excel.
        """
        if (not isinstance(columns, list)) or (len(columns) < 2):
            raise Exception(f'[{__class__.__name__}] -> para concatenar informe pelo menos duas colunas válidas.')
        
        # Pegar os índices das colunas informadas.
        index_list = []
        for text in columns:
            if not isinstance(text, str):
                raise Exception(f'[{__class__.__name__}] -> para concatenar uma lista de linhas os elementos informados na coluna precisam ser do tipo str é não {type(text)}')

            if not self.hederToRow().containsIqual(text):
                continue # Pular texto inexistente no cabeçalho.

            # Incluir índice na lista
            index_list.append(self.header.index(text))

        # Concatenar as posições e gerar um novo elemento.
        new_header = self.hederToRow().concatItems(index_list)
        self.header = new_header

        # Concatenar cada linha
        line: SheetRow = None
        for line in self.lines:
            line.concatItems(index_list)
    
    def findLinesInColumn(self, *, column: str, text: str) -> list:
        """
            Retorna todas as linhas que contenham a ocorrência [text] na coluna [column]
        Na comparação, a correspondêcia deve ser exata [==]
        Retorna uma lista de objetos SheetRow()
        """
        lines_math: list = [self.hederToRow()]
        num_index_column: int = self.indexInColumn(column) # Posição do item na coluna
        if num_index_column == -1:
            stdout_util.print_line('-')
            print(f'[{__class__.__name__}] ERRO a coluna [{column}] não existe no cabeçalho da planilha')
            stdout_util.print_line('-')
            return lines_math
        
        row: SheetRow = None
        for row in self.lines:
            if text == row.getElementAt(num_index_column).toString():
                lines_math.append(row)
        return lines_math
    
    def findContainsLineInColumn(self, *, column: str, text: str) -> list: # list<SheetRow>
        """
            Retorna todas as linhas que contenham a ocorrência [text] na coluna [column]
        Na comparação, a correspondêcia deve contains.
        Retorna uma lista de objetos SheetRow()
        """
        lines_math: list = [self.hederToRow()]
        num_index_column: int = self.indexInColumn(column) # Posição do item na coluna
        if num_index_column == -1:
            return lines_math
        
        row: SheetRow = None
        for row in self.lines:
            if text in row.getElementAt(num_index_column).toString():
                lines_math.append(row)
        return lines_math

    def removeColumn(self, column_name: str) -> bool:
        if self.indexInColumn(column_name) == -1:
            return False
        
        num_index = self.header.index(column_name)
        new_list_rows = []
        row: SheetRow = None
        for row in self.lines:
            row.removeItemAt(num_index)
            new_list_rows.append(row)

        self.lines = new_list_rows
        h: SheetRow = self.hederToRow()
        h.removeItemIqual(column_name)
        self.header = h.sheet_line
        return True
    

#===========================================================#
# Parse do arquivo
#===========================================================#
class NullSheet(object):
    def __init__(self, data_frame: DataFrame) -> None:
        self.data_frame: DataFrame = data_frame
        self.__num_lines:int = None

    def getHeader(self) -> list:
        """Retorna a primeira lina da planilha"""
        return []
    
    def getHeaderString(self) -> str:
        return ''

    def getValues(self) -> ndarray:
        pass
    
    def toList(self) -> list:
        return []
    
    def getNumLines(self) -> int:
        return 0
    
    def getNumColumns(self) -> int:
        return 0

    def toSheetRows(self) -> list:
        return []


class ParseFileSheet(NullSheet):
    def __init__(self, *,data_frame: DataFrame, separator=' ') -> None:
        self.data_frame: DataFrame = data_frame
        self.separator: str = separator
        self.__num_lines:int = None

        if not isinstance(self.data_frame, DataFrame):
            print('ERRO -> DataFrame inválido')
            sys.exit(1)

    def getHeader(self) -> list:
        """Retorna a primeira lina da planilha"""
        return self.data_frame.columns.tolist()
    
    def getHeaderString(self) -> str:
        return self.separator.join(self.getHeader())
    
    def getHeaderRow(self) -> SheetRow:
        return SheetRow(sheet_line=self.getHeader(), num_line=0, separator=self.separator)

    def getValues(self) -> ndarray:
        return self.data_frame.values
    
    def toList(self) -> list:
        return self.getValues().tolist()
    
    def getNumLines(self) -> int:
        if self.__num_lines is None:
            self.__num_lines = len(self.toList())
        return self.__num_lines
    
    def getNumColumns(self) -> int:
        return len(self.getHeader())

    def toSheetRows(self) -> list: # list<SheetRows>
        """
            Retorna uma lista de objetos do tipo SheetRow
        """
        _list_rows: list = []
        row: SheetRow = None
        for num, line in enumerate(self.toList()):
            row = SheetRow(sheet_line=line, num_line=num, separator=self.separator)
            _list_rows.append(row)
        return _list_rows
    

def getDataFrameFromFile(filename: str) -> DataFrame:
    """Detectar o tipo de arquivo e retornar um DataFrame do arquivo"""
    if filename is None:
        return DataFrame()
    
    if not os.path.isfile(filename):
        return DataFrame()
    
    fp: FilePath = FilePath(filename)
    
    stdout_util.print_line('-')
    print(f'Obtendo DataFrame do arquivo -> {filename}')
    if fp.extension().lower() == '.csv':
        return pandas.read_csv(filename)
    elif fp.extension().lower() == '.xlsx':
        return pandas.read_excel(filename)
    elif fp.extension().lower() == 'xls':
        return pandas.read_excel(filename)
    elif fp.extension().lower() == '.json':
        return pandas.read_json(filename)

    return DataFrame()


def getParseSheetFromFile(*, filename: str, separator: str=' ') -> ParseFileSheet:
    _d: DataFrame = getDataFrameFromFile(filename)
    if _d.empty:
        print(f'[ERRO] o DataFrame é vazio -> {filename}')
        return NullSheet(_d)
    return ParseFileSheet(data_frame=_d, separator=separator)


#===========================================================#
# FIND TEXT
#===========================================================#
class FindTextInList(object):
    def __init__(self, *, header:list, lines_list:list, column:str, text:str) -> None:
        # lines_list<list>
        self.header: list = header
        self.lines_list: list = lines_list
        self.column: str = column # Coluna em que se deve filtrar o texto.
        self.text: str = text # Texto a ser buscado/filtrado.
        self.__list_math = []
     
    def indexColumn(self) -> int:
        try:
            return self.header.index(self.column)
        except:
            print(f'{__class__.__name__} -> a coluna {self.column} não existe no cabeçalho')
            return -1 # ERRO
        
    def toList(self) -> list:
        """
            Retorna uma lista com todas as ocorrências encontradas
        """
        if self.__list_math != []:
            return self.__list_math

        if self.lines_list == []:
            return []
        
        num_index = self.indexColumn()
        if num_index == -1:
            return []
        
        for line in self.lines_list:
            try:
                if self.text == str(line[num_index]):
                    self.__list_math.append(line)
            except:
                pass
        return self.__list_math
    

class FindTextIRows(object):  
    """
        Classe para filtrar texto em linhas do tipo ListRows()
    
    Basta instânciar a classe ListRows com as  linhas que deseja filtrar,
    em seguida instânciar está classe e usar os métodos para filtro de dados.
    """         
    def __init__(self, *, rows_list:ListRows, column:str, text:str, separator='\t') -> None:
        if not isinstance(rows_list, ListRows):
            raise Exception(f'[{__class__.__name__}] -> rows_list precisa ser uma instância de ListRows')

        self.rows_list: ListRows = rows_list
        self.column: str = column
        self.text: str = text
        self.separator: str = separator
        self.__list_math_rows = []

    def findIqualToRows(self) -> list: # list<SheetRow>
        if self.__list_math_rows == []:
            self.__list_math_rows = self.rows_list.findLinesInColumn(column=self.column, text=self.text)
        return self.__list_math_rows
    
    def findIqualToList(self) -> list: # list()
        l = []
        item: SheetRow = None
        for item in self.findIqualToRows():
            l.append(item.sheet_line)
        return l
    
    def findIqualToDataFrame(self) -> DataFrame:
        return DataFrame(self.findIqualToList())
        
    def findIqualToCsv(self, file_path: FilePath) -> bool:
        if not self.checkOutPutFile(file_path):
            return False
        
        if file_path.extension() != '.csv':
            print(f'{__class__.__name__} -> o arquivo de saída não é do tipo .csv')
            return False
        
        print(f'[EXPORTANDO] -> {file_path.absolute()}')
        self.findIqualToDataFrame().to_csv(file_path.absolute(), sep=self.separator, header=False)

    def findIqualToExcel(self, file_path: FilePath) -> bool:
        if not self.checkOutPutFile(file_path):
            return False
        
        if file_path.extension() != '.xlsx':
            print(f'{__class__.__name__} -> o arquivo de saída não é do tipo .xlsx')
            return False
        
        print(f'[EXPORTANDO] -> {file_path.absolute()}')
        self.findIqualToDataFrame().to_excel(file_path.absolute(), header=False)

    def findIqualToJson(self, file_path: FilePath):
        if not self.checkOutPutFile(file_path):
            return False
        
        if file_path.extension() != '.json':
            print(f'{__class__.__name__} -> o arquivo de saída não é do tipo .json')
            return False
        
        print(f'[EXPORTANDO] -> {file_path.absolute()}')
        self.findIqualToDataFrame().to_json(file_path.absolute(), indent=4)

    def findContains(self) -> list: # list<SheetRow>
        return self.rows_list.findContainsLineInColumn(column=self.column, text=self.text)

    def checkOutPutFile(self, output_file: FilePath) -> bool:
        if not isinstance(output_file, FilePath):
            print(f'{__class__.__name__} -> o arquivo de saída precisa ser do tipo FilePath')
            return False
        
        if output_file.path.exists():
            print(f'[PULANDO] -> o arquivo já existe -> {output_file.absolute()}')
            return False
        
        if not os.path.isdir(output_file.dirname()):
            print(f'{__class__.__name__} -> o diretório não existe {output_file.dirname()}')
            return True
        
        return True


class BuildFindTextInRows(object):
    def __init__(self) -> None:
        self._rows_list:ListRows = None
        self._column:str = None 
        self.text:str = None 
        self._separator='\t'

    @property
    def text(self):
        return self._text
    
    @text.setter
    def text(self, new_text):
        self._text = new_text

    def set_list_rows(self, new_list_rows):
        if not isinstance(new_list_rows, ListRows):
            print(f'{__class__.__name__} -> as linhas de FindText precisam ser do tipo ListRows')
            return self
        self._rows_list = new_list_rows

    def set_column(self, new_column):
        if not isinstance(new_column, str):
            print(f'{__class__.__name__} a coluna precisa ser do tipo str')
            return self
        self._column = new_column
        return self

    def set_text(self, new_text):
        if not isinstance(new_text, str):
            print(f'{__class__.__name__} o texto precisa ser do tipo str')
            return self
        self.text = new_text
        return self

    def set_separator(self, new_separator):
        self._separator = new_separator
        return self
    
    def check(self) -> bool:
        if self._rows_list is None:
            print(f'{__class__.__name__} rows_list não foi definido')
            return False
        elif self._column is None:
            print(f'{__class__.__name__} column não foi definido')
            return False
        
        elif self.text is None:
            print(f'{__class__.__name__} text não foi definido')
            return False
        return True

    def build(self) -> FindTextIRows:
        
        if not self.check():
            sys.exit(1)
        
        return FindTextIRows(
            rows_list=self._rows_list,
            column=self._column,
            text=self.text,
            separator=self._separator,
        )

#===========================================================#
# COMMANDS
#===========================================================#

class CommandFind(object):
    def __init__(self, *, find_text: FindTextIRows) -> None:
        self.find_text: FindTextIRows = find_text
        self.output_filename: str = f'out-{self.find_text.text}'

    def execute(self):
        pass

class CommandFindExport(CommandFind):
    def __init__(self, *, find_text: FindTextIRows, outputdir: str, type_file: str='excel') -> None:
        super().__init__(find_text=find_text)
        self.outputdir: str = outputdir
        self.type_file: str = type_file

    def execute(self):
        """
            Filtrar e exportar
        """
        out = f'{self.outputdir}{os.sep}{self.output_filename}'
        if self.type_file == 'csv':
            self.find_text.findIqualToCsv(FilePath(f'{out}.csv'))
        elif self.type_file == 'json':
            self.find_text.findIqualToJson(FilePath(f'{out}.json'))
        else:
            self.find_text.findIqualToExcel(FilePath(f'{out}.xlsx'))
                    

class ExecuteCommands(object):
    def __init__(self) -> None:
        self.commands_list: list = []

    def add_command(self, command: CommandFindExport):
        if not isinstance(command, CommandFindExport):
            print('Comando inválido')
            return
        self.commands_list.append(command)
    def execute(self):
        c: CommandFindExport = None
        for c in self.commands_list:
            c.execute()



#===========================================================#
# RUN ACTIONS
#===========================================================#
class UserOptions(object):
    def __init__(self, *, user_options: dict) -> None:
        #
        # -i|--input   -> input_file
        # -o|--output  -> output_file
        # -c|--column  -> column
        # -e|--export  -> type_file <csv>|<excel>|<json>
        # --find       -> find
        # --finds      -> finds
        # --header     -> header
        # --del-column -> del_column
        # --sep        -> separator
        #
        self.user_options: dict = user_options

    def containsArg(self, arg: str):
        if self.user_options[arg] is None:
            return False
        return True

    def isFinds(self) -> bool:
        if self.user_options['finds'] is None:
            return False
        return True
    
    def getSeparator(self):
        if self.user_options['separator'] is None:
            return '\t'
        return self.user_options['separator'][0]
    
    def getExport(self):
        if self.user_options['type_file'] is None:
            return 'excel'
        else:
            return self.user_options['type_file'][0]

    def isDelColumn(self) -> bool:
        if self.user_options['del_column'] is None:
            return False
        return True

    def getDelColumn(self):
        return self.user_options['del_column'][0]

    def isHeader(self) -> bool:
        return self.user_options['header']

    def getFinds(self):
        return self.user_options['finds']

    def getFind(self):
        return self.user_options['find'][0]
        
    def containsInputFile(self) -> bool:
        return self.containsArg('input_file')
    
    def getFileInput(self) -> FilePath:
        return FilePath(self.user_options['input_file'][0])
    
    def getFileOutput(self) -> FilePath:
        return FilePath(self.user_options['output_file'][0])
    
    def getColumn(self) -> str:
        return self.user_options['column'][0]


def actionShowHeader(*, content_options: UserOptions, separator=' '):
    if not content_options.containsInputFile():
        print('Necessário um arquivo input')
        sys.exit(1)

    if not content_options.getFileInput().path.is_file():
        print(f'o arquivo não existe -> {content_options.getFileInput().absolute()}')
        sys.exit(1)

    parse: ParseFileSheet = getParseSheetFromFile(
                                                filename=content_options.getFileInput().absolute(),
                                                separator=separator,
                                                )
    
    print(parse.getHeader())


def actionDeleteColumn(*, content_options: UserOptions) -> bool:
    if content_options.getDelColumn() is None:
        print('Necessário informar uma coluna para executar esta ação')
        return False
    
    if content_options.getFileOutput() is None:
        print('Necessário informar um aquivo de saída')
        return False

    if content_options.getSeparator() is None:
        separator = '\t'
    else:
        separator = content_options.getSeparator()

    parse: ParseFileSheet = getParseSheetFromFile(
            filename=content_options.getFileInput().absolute(),
            separator=separator
    )

    print(f'[DELETANDO COLUNA] -> {content_options.getDelColumn()}')
    # Instânciar o objeto ListRows com as linhas do arquivo/planilha.
    rows: ListRows = ListRows(
                            header=parse.getHeader(), 
                            lines=parse.toSheetRows(), 
                            separator=content_options.getSeparator()
                            )

    rows.removeColumn(content_options.getDelColumn())
    new_lines = [rows.header]
    row: SheetRow = None
    for row in rows.lines:
        new_lines.append(row.sheet_line)

    df: DataFrame = DataFrame(new_lines)
    print(f'[EXPORTANDO] -> {content_options.getFileOutput().absolute()}')
    if content_options.getExport() == 'excel':
        df.to_excel(content_options.getFileOutput().absolute())
    elif content_options.getExport() == 'csv':
        df.to_csv(content_options.getFileOutput().absolute(), header=False, sep=content_options.getSeparator())
    elif content_options.getExport() == 'json':
        df.to_json(content_options.getFileOutput().absolute(), indent=4, force_ascii=True)

    return True

def actionFilter(*, options: UserOptions):
    pass

def actionFilters(*, options: UserOptions) -> bool:
    if not options.containsInputFile():
        print('Necessário informar um arquivo input')
        return False
    
    if options.getColumn() is None:
        print('Necessário informar uma coluna')
        return False    
    
    # Ler as linhas da planilha excel/csv
    parse: ParseFileSheet = getParseSheetFromFile(
                                                    filename=options.getFileInput().absolute(),
                                                    separator=options.getSeparator()
                                                  )
    
    rows: ListRows = ListRows(
        header=parse.getHeader(),
        lines=parse.toSheetRows(),
    )
    
    # Criar filtro para os itens.
    build_find: BuildFindTextInRows = BuildFindTextInRows()
    build_find.set_separator(parse.separator)
    build_find.set_column(options.getColumn())
    build_find.set_list_rows(rows)

    # Criar os comandos.
    exec_commands: ExecuteCommands = ExecuteCommands()
    output_dir = options.getFileInput().dirname()
    for f in options.getFinds():
        exec_commands.add_command(
                                CommandFindExport(
                                                outputdir=output_dir, 
                                                find_text=build_find.set_text(f).build()
                                                )
                                )
        
    # Exportar arquivos com a execução dos comandos.
    exec_commands.execute()


def appActions(*, options: dict) -> bool:
    if not isinstance(options, dict):
        print(f'[ERRO] as opções precisam ser do tipo dict')
        return False
    
    input_options: UserOptions = UserOptions(user_options=options)

    if input_options.isHeader():
        actionShowHeader(content_options=input_options)
        return True
    elif input_options.isDelColumn():
        actionDeleteColumn(content_options=input_options)
    elif input_options.isFinds():
        actionFilters(options=input_options)


def main():
    pass

    
if __name__ == '__main__':
    main()