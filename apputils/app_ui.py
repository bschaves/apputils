#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Este programa e um GUI gráfico que usa o youtube-dl para baixar vídes do youtube.

Configuração:
   Debian/Ubuntu
	  $ sudo apt install python3-pyqt5
	    

   Linux
	  $ pip3 install PyQt5

   Windows
	  $ pip install PyQt5
'''

'''
HISTÍRICO DE VERSÕES

(v0.1.2) - 2021-06-01
   Corrigir erro que encerrava o programa ao clicar na opção de Baixar, a solução
TEMPORÁRIA foi usar o app youtube-dl em uma pasta isolada (a pasta de cache do youtube-dl-qt).

(v0.1.1 - 2021-05-28)
   Corrigir erro no arquivo configure.py que encerrava o aplicativo o erro acontecia 
ao tentar atribuir o caminho de download de vídeos - a solução TEMPORÁRIA foi usar o 
diretório de downloads do usuário como caminho padrão ao invés da HOME.
'''

import os, sys
import subprocess
from threading import Thread
from pathlib import Path

try:
    from PyQt5.QtWidgets import (
        QApplication, QWidget, QMainWindow,
        QMessageBox, QFileDialog, QLineEdit,
        QLabel, QProgressBar, QPushButton,
        QVBoxLayout, QHBoxLayout, QGridLayout,
        QGroupBox, QComboBox, QAction,
    )
    from PyQt5.QtGui import QIcon
    from PyQt5.QtCore import Qt

except Exception as err:
    print(err)
    sys.exit(1)

_script = os.path.realpath(__file__)
dir_of_executable = os.path.dirname(_script)
sys.path.insert(0, dir_of_executable)

__version__ = '0.1.0'
__author__ = 'Bruno Chaves'
__repo__ = ''



class MessageWindow(QWidget):
    '''
	https://doc.qt.io/qtforpython/PySide2/QtWidgets/QMessageBox.html
	https://stackoverflow.com/questions/40227047/python-pyqt5-how-to-show-an-error-message-with-pyqt5
	'''

    def __init__(self):
        super().__init__()
        self.msgBox = QMessageBox()

    def msgOK(self, text: str):
        self.msgBox.setText(text)
        self.msgBox.exec()

    def msgError(self, text=''):
        self.msgBox.setIcon(QMessageBox.Critical)
        self.msgBox.setText(text)
        # self.msg.setInformativeText('More information')
        self.msgBox.setWindowTitle("Error")
        self.msgBox.exec_()



class AppMainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle('Stream Texto')
        self.setGeometry(250, 150, 670, 400)
        #self.setFixedSize(670, 400)

        self.main_container = QWidget()
        self.setCentralWidget(self.main_container)

        self.external_widgets = ContentWidget(self.main_container)
        self.grid_master = QGridLayout(self)
        self.setupUI()

    def setupUI(self):
        self.setupTopBar()
        self.grid_master.addWidget(self.external_widgets, 0, 0)
        self.setLayout(self.grid_master)
        self.show()

    def setupTopBar(self):
        self.statusBar()
        menubar = self.menuBar()

        # === Menu Arquivo ===#
        fileMenu = menubar.addMenu('&Arquivo')

        # Ações do menu arquivo
        exitAct = QAction(QIcon('exit.png'), '&Sair', self)
        exitAct.setShortcut('Ctrl+Q')
        exitAct.setStatusTip('Sair do programa')
        exitAct.triggered.connect(self.external_widgets.clickExit)
        fileMenu.addAction(exitAct)

        aboutMenu = menubar.addMenu('&Sobre')
        versionMenu = aboutMenu.addMenu('Versão')
        versionMenu.addAction(__version__)
        authorMenu = aboutMenu.addMenu('Autor')
        authorMenu.addAction(__author__)
        siteMenu = aboutMenu.addMenu('Site')
        siteMenu.addAction(__repo__)



class ContentWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self._min_width = 650
        self._min_height = 180
        self.__total_urls = 0

        # Container superior
        self.group_box_urls = QGroupBox('Insira URLs abaixo')

        # Container central
        self.group_box_select_files = QGroupBox('Selecione uma planilha')

        # Container inferior
        self.group_box_download = QGroupBox('Download')

        self.group_box_master = QGroupBox()

        # Containers de posicionamento.
        self.grid_layout_urls = QGridLayout()
        self.layout_horizontal_dir_downloads = QHBoxLayout()
        self.grid_select_files = QGridLayout() #QVBoxLayout()
        self.grid_layout_progress = QGridLayout()
        self.grid_master = QGridLayout()

        self.video_formats = ['opção1', 'opção2']
        self.dir_download_text = os.path.abspath((Path().home()))
        self.path_output_file = '/home/exemplo'

        self.setupUI()

    def setupUI(self):
        # Definir Widigets do primeiro grupo
        self.line_edit_url = QLineEdit(self, placeholderText='URL aqui')
        self.line_edit_url.setFixedSize(550, 50)
        self.btn_add_url = QPushButton('Adicionar', self)
        self.btn_add_url.setFixedSize(70, 50)
        self.btn_add_url.clicked.connect(self.add_url)

        # Inserir os widgets superiores no primeiro grid
        self.grid_layout_urls.addWidget(self.line_edit_url, 0, 0)
        self.grid_layout_urls.addWidget(self.btn_add_url, 0, 1)

        # Inserir o grid layout no groupbox superior
        self.group_box_urls.setLayout(self.grid_layout_urls)
        self.group_box_urls.setFixedSize(self._min_width, 90)

        #============================================================#
        # Widigets do segundo grupo
        # Botões para seleção de arquivos.
        #============================================================#

        self.btn_select_destination = QPushButton('Alterar pasta', self)
        self.btn_select_destination.setFixedSize(130, 30)
        self.btn_select_destination.clicked.connect(self.selectFolder)

        self.btn_select_output_file = QPushButton('Arquivo de saída', self)
        self.label_path_output_file = QLabel(self.path_output_file, self)

        # Exibir o texto do diretório de download.
        self.label_dir_download = QLabel(self.dir_download_text, self)
        self.label_dir_download.setFixedSize(250, 30)

        # Seleção de itens
        self.combo_video_formats = QComboBox()
        self.combo_video_formats.addItems(self.video_formats)

        # Incluir os botões em um GridLayout
        self.grid_select_files.addWidget(self.btn_select_destination, 0, 0)
        self.grid_select_files.addWidget(self.label_dir_download, 0, 1)
        self.grid_select_files.addWidget(self.btn_select_output_file, 1, 0)
        self.grid_select_files.addWidget(self.label_path_output_file, 1, 1)
        self.grid_select_files.addWidget(self.combo_video_formats, 2, 0)
        

        # Definir o grupbox para os botões de seleção de arquivos.
        self.group_box_select_files.setLayout(self.grid_select_files)
        
        #self.group_box_select_dir.setFixedSize(self._min_width, 90)

        #============================================================#
        # Adicionar o terceiro grupo de widgtes.
        #============================================================#
        self.btn_download = QPushButton('Baixar', self)
        self.btn_download.clicked.connect(self.thread_download)

        self.pbar = QProgressBar(self)
        self.grid_layout_progress.addWidget(self.btn_download)
        self.grid_layout_progress.addWidget(self.pbar)
        self.group_box_download.setLayout(self.grid_layout_progress)

        #============================================================#
        # Adicionar todos os grupos na janela principal
        #============================================================#
        self.grid_master.addWidget(self.group_box_urls, 0, 0)
        self.grid_master.addWidget(self.group_box_select_files, 1, 0)
        self.grid_master.addWidget(self.group_box_download, 2, 0)
        self.setLayout(self.grid_master)

    

    def get_url(self):
        """Retornar o URL da caixa line edit."""
        return self.line_edit_url.text()

    def thread_download(self):
        """Executa a o download em uma thread."""
        Thread(target=self.run_download).start()

    def get_subprocess(self) -> object:
        """Cria um subprocesso com youtube-dl e retorna o objeto apartir de subprocess.Popen()"""
        pass

    def run_download(self):
        """"
		Método para baixar o vídeo e mostrar o progresso de download.
		youtube-dl --no-playlist --continue --format mp4 -o "%(title)s.%(ext)s" URL
		"""

        # self.pbar.setValue(0)
        print()
        MessageWindow().msgOK('Download(s) finalizado(s)')

    def add_url(self):
        '''
		Método para obter o url digitado na caixa de urls e inserir na lista 'self.list_urls'
		'''
        pass

    def selectFolder(self):
        '''
		Usar janela do navegador de arquivos para selecionar uma pasta de destino
		para download dos vídeos.
		'''
        select_dir = QFileDialog.getExistingDirectory(
            None,
            'Selecione um diretório',
            self.dir_download_text,
            QFileDialog.ShowDirsOnly
        )

        self.dir_download_text = select_dir  # Alterar o atributo.
        self.label_dir_download.setText(f'Salvar em: {self.dir_download_text}')

    def createFile(self):
        '''
		Este método está NÃO está em uso no momento.
		serve para selecionar o nome do arquivo antes de baixar.
		'''
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName = QFileDialog.getSaveFileName(
            self, "QFileDialog.getSaveFileName()",
            "",
            "Video Files (*.mp4);;All Files (*)",
            options=options)
        if fileName:
            f = fileName[0].strip().replace(' ', '_')
            return f
        else:
            return None

    def openFileNameDialog(self):
        '''
		Caixa de seleção de arquivo.
		'''
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                  "All Files (*);;Python Files (*.py)", options=options)
        if fileName:
            return fileName
        else:
            return False

    def clickExit(self):
        sys.exit(0)


def main():
    app = QApplication(sys.argv)
    window = AppMainWindow()
    window.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    main()
